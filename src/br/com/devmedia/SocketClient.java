package br.com.devmedia;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import java.util.Date;

public class SocketClient {
    private static final String HOST = "127.0.0.1";
    private static final int PORT = 1234;
    private static final int FILE_SIZE = 6022386;
    
    public static void main(String[] args) {
        try {
            Socket socket = new Socket(HOST, PORT);
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            
//            String message = "Cliente - Conectado às " + new Date();
//            
//            System.out.println("Cliente: Mensagem enviada => \"" + message + "\"");
//            
//            dos.writeUTF(message);
//            
//            message = dis.readUTF();
//            
//            System.out.println("Cliente: Mensagem recebida => \"" + message + "\"");

            byte[] bArray = new byte[FILE_SIZE];
            
            FileOutputStream fos = new FileOutputStream("C:\\Users\\PINHEIRO\\Desktop\\mario_copy.png");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            int bytesRead = dis.read(bArray, 0, bArray.length);
            int current = bytesRead;
            
            do {
                bytesRead = dis.read(bArray, current, (bArray.length - current));
                if (bytesRead >= 0) {
                    current += bytesRead;
                }
            } while (bytesRead > -1);
            
            bos.write(bArray, 0, current);
            
            System.out.println("Arquivo recebido com sucesso!");
            
            bos.flush();
            
            bos.close();
            dis.close();
            dos.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}